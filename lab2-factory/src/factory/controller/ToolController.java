package factory.controller;

import factory.model.DigitalSignal;
import factory.model.WidgetKind;
import factory.swingview.Factory;

public class ToolController {
  private final DigitalSignal conveyor, press, paint;
  private final long pressingMillis, paintingMillis;
  private boolean isPressing = false;
  private boolean isPainting = false;

  public ToolController  (DigitalSignal conveyor,
                          DigitalSignal press,
                          DigitalSignal paint,
                          long pressingMillis,
                          long paintingMillis)
  {
    this.conveyor = conveyor;
    this.press = press;
    this.paint = paint;
    this.pressingMillis = pressingMillis;
    this.paintingMillis = paintingMillis;
  }

  public synchronized void onPressSensorHigh(WidgetKind widgetKind) throws InterruptedException {
    if (widgetKind == WidgetKind.BLUE_RECTANGULAR_WIDGET) {
      startPress();
      waitOutside(pressingMillis);
      press.off();
      waitOutside(pressingMillis);     // press needs time to retract
      notifyPressOff();
    }
  }

  private void startPress() {
    isPressing = true;
    conveyor.off();
    press.on();
  }

  private void notifyPressOff() throws InterruptedException {
    isPressing = false;
    notifyAll();
    while (isPainting) {
      wait();
    }
    conveyor.on();
  }

  public synchronized void onPaintSensorHigh(WidgetKind widgetKind) throws InterruptedException {
    if (widgetKind == WidgetKind.ORANGE_ROUND_WIDGET) {
      startPaint();
      paint.off();
      notifyPaintOff();
    }
  }

  private void startPaint() throws InterruptedException {
    isPainting = true;
    conveyor.off();
    paint.on();
    waitOutside(paintingMillis);
  }

  private void notifyPaintOff() throws InterruptedException {
    isPainting = false;
    notifyAll();
    while (isPressing) {
      wait();
    }
    conveyor.on();
  }

  // Helper method: sleep outside of monitor for 'millis' milliseconds.
  private void waitOutside(long millis) throws InterruptedException {
    long timeToWakeUp = System.currentTimeMillis() + millis;
    long actualTime = System.currentTimeMillis();
    while (timeToWakeUp > actualTime) {
      long dt = timeToWakeUp - actualTime;
      wait(dt);
      actualTime = System.currentTimeMillis();
    }
  }

  public static void main(String[] args) {
    Factory factory = new Factory();
    factory.startSimulation();
  }
}
