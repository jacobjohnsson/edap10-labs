I2:
  > Det är säkert när båda robotarmar är färdiga med sitt arbete.

I7:
  > "However, the simulation should demonstrate good throughput: concurrent pressing and painting should once again be possible."  
  Men det gör den inte. conveyor.on() anropas för tidigt när båda robotarmar körs samtidigt.

  > Genomströmningen förändras eftersom wait kan "vakna" innan den satta tiden. Thread.sleep(time) kommer  garanterat "sova" minst time millisekunder.


### Reflektion
  1. För att vi inte behövde göra flera saker samtidigt.

  2. race condition var mellan vilken metod som blev färdig först.

  3. Inget kunde ske samtidigt. Pressen var tvungen att vänta på att målandet skulle bli färdigt eller vice verse.

  4. wait(time) kan väckas tidigare och väntar dessutom max time millisekunder. Thread.sleep(time) däremot sover MINST time millisekunder.

  5. * __while (condition) { wait(); }__ kommer alltid att kolla villkoret när anropet till wait() returnerar.  
     * __if (condition) { wait(); }__ kommer kolla villkoret den första gången men när man kommer tillbaka från wait() anropet kommer man helt enkelt att lämna blocket.
