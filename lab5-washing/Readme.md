# Lab 5

### P5
| Order | Sender | Receiver | Sends Ack back |
| -- | -- | -- | -- |
| SPIN_OFF | Program | Spin | X |
| SPIN_SLOW | Program | Spin |  |
| SPIN_FAST | Program | Spin |  |
| TEMP_IDLE | Program | Temp |  |
| TEMP_SET | Program | Temp |  |
| WATER_IDLE | Program | Water |  |
| WATER_FILL | Program | Water | X |
| WATER_DRAIN | Program | Water | X |
| ACKNOWLEDGMENT | temp, water and spin | Sender (Program) |  |

### P6
if dt = 10 then mu = 0.478 and ml = 9.52 * 10^-3, see 5.1.4 in the instructions.

## Reflection
 * main, program, io, water, spin, temp
 * The threads only communicate through the program thread. Temp, water and spin never directly talk to eachother, they don't even know about eachother. There is NO shared data.
 * um..?
 * Yes, every MessagingThread(program, water, spin, temp) has a BlockingQueue for receiving messages.
 * Every program is responsible for its own interrupt behavior. Our programs simply tell the system to go into idle mode.
 * By designing the programs correctly using acknowledgements. In this case we tell the waterController to send an acknowledgement when it is done.
 * By waiting for an acknowledgement from the temperature thread.
