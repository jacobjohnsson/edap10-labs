package lab;

import wash.WashingIO;

public class WaterController extends AbstractController {

  private double currentLevel;
  private double target;

  private enum Flow {IDLE, FILLING, DRAINING};

  private Flow state = Flow.IDLE;
  private MessagingThread lastSender;

  public WaterController(WashingIO io) {
    super(io, 2000);
    currentLevel = io.getWaterLevel();
  }

  @Override
  protected void handleCommand(WashingMessage m) {
    lastSender = m.getSender();
    switch (m.getCommand()) {

      case WashingMessage.WATER_IDLE:
        currentLevel = io.getWaterLevel();
        target = currentLevel;
        state = Flow.IDLE;
        io.fill(false);
        io.drain(false);
        break;

      case WashingMessage.WATER_FILL:
        target = m.getValue();
        state = Flow.FILLING;

        break;

      case WashingMessage.WATER_DRAIN:
        target = 0;
        state = Flow.DRAINING;
        break;
    }
  }

  @Override
  protected void handleState() {
    currentLevel = io.getWaterLevel();

    switch (state) {
      case IDLE:
        // Simply chill and enjoy life
        break;

      case FILLING:
        if (currentLevel <= target) {
          io.fill(true);
        } else {
          io.fill(false);
          state = Flow.IDLE;
          lastSender.send(new WashingMessage(this, WashingMessage.ACKNOWLEDGMENT));
        }
        break;

      case DRAINING:
        if (currentLevel > target) {
          io.drain(true);
        } else {
          io.drain(false);
          state = Flow.IDLE;
          lastSender.send(new WashingMessage(this, WashingMessage.ACKNOWLEDGMENT));
        }
        break;
    }
  }
}
