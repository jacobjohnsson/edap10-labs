package lab;
import simulator.WashingSimulator;
import wash.WashingIO;

public class Wash {

  // simulation speed-up factor:
  // 50 means the simulation is 50 times faster than real time
  public static final int SPEEDUP = 50;

  public static void main(String[] args) throws InterruptedException {
    WashingSimulator sim = new WashingSimulator(SPEEDUP);

    WashingIO io = sim.startSimulation();

    TemperatureController temp = new TemperatureController(io);
    WaterController water = new WaterController(io);
    SpinController spin = new SpinController(io);
    Thread currentProgram = new NoProgram();

    temp.start();
    water.start();
    spin.start();

    while (true) {
      int n = io.awaitButton();
      System.out.println("user selected program " + n);

      switch (n) {
        case 0:
          currentProgram.interrupt();
          currentProgram = new NoProgram();
          break;
        case 1:
          currentProgram = new ColorWash(io, temp, water, spin);
          break;
        case 2:
          currentProgram = new WhiteWash(io, temp, water, spin);
          break;
        case 3:
          currentProgram = new Drain(io, temp, water, spin);
          break;
      }
      currentProgram.start();
    }
  }
}
