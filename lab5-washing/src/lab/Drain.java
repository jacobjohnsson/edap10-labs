package lab;

import wash.WashingIO;

/**
 * Program 3 for washing machine.
 * Serves as an example of how washing programs are structured.
 *
 * This short program stops all regulation of temperature and water
 * levels, stops the barrel from spinning, and drains the machine
 * of water.
 *
 * It is can be used after an emergency stop (program 0) or a
 * power failure.
 */
class Drain extends AbstractWashingProgram {
  public Drain(WashingIO io,
               MessagingThread<WashingMessage> temp,
               MessagingThread<WashingMessage> water,
               MessagingThread<WashingMessage> spin) {
    super(io, temp, water, spin, "Drain");
  }

  @Override
  public void program() throws InterruptedException {

    // Switch off heating
    temp.send(new WashingMessage(this, WashingMessage.TEMP_IDLE));

    // Switch off spin
    spin.send(new WashingMessage(this, WashingMessage.SPIN_OFF));

    // Drain barrel (may take some time)
    water.send(new WashingMessage(this, WashingMessage.WATER_DRAIN));
    WashingMessage ack = receive();  // wait for acknowledgment
    System.out.println("got " + ack);
    water.send(new WashingMessage(this, WashingMessage.WATER_IDLE));

    // Unlock hatch
    io.lock(false);
  }
}
