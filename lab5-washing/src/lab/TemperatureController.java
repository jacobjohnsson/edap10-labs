package lab;

import wash.WashingIO;

public class TemperatureController extends AbstractController {

  private double target;
  private double currentTemp;
  private int isHeating = 0;

  private static final double mu = 0.7;   // non-conservative 0.478
  private static final double ml = 0.1;   // non-conservative 0.00952

  public TemperatureController(WashingIO io) {
    super(io, 10000);
    currentTemp = io.getTemperature();
  }

  @Override
  protected void handleCommand(WashingMessage m) {
    switch (m.getCommand()) {
      case WashingMessage.TEMP_IDLE:
        target = 0;
        break;
      case WashingMessage.TEMP_SET:
        target = m.getValue();
        currentTemp = io.getTemperature();
        if (currentTemp < (target - mu)) {
          isHeating = 1;
        } else {
          isHeating = 0;
        }
        break;
    }
  }

  @Override
  protected void handleState() {
    currentTemp = io.getTemperature();

    // Påväg upp, else => påväg ner
    switch (isHeating) {
      case 1:
        if (shouldHeatOnWayUp()) {
          io.heat(true);
        } else {
          io.heat(false);
          isHeating = 0;
        }
      break;

      case 0:
        if (shouldHeatOnWayDown()) {
          io.heat(true);
          isHeating = 1;
        } else {
          io.heat(false);
        }
        break;
    }
  }

  private boolean shouldHeatOnWayUp() {
    return currentTemp < (target - mu);
  }

  private boolean shouldHeatOnWayDown() {
    return currentTemp < (target - 2 + ml);
  }
}
