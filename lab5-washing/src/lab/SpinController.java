package lab;

import wash.WashingIO;

public class SpinController extends AbstractController {

  private enum SpinDirection {LEFT, RIGHT};
  private enum Speed {IDLE, SLOW, FAST};

  private SpinDirection dir = SpinDirection.LEFT;
  private Speed speed = Speed.IDLE;

  public SpinController(WashingIO io) {
    super(io, 60000);
  }

  @Override
  protected void handleCommand(WashingMessage m) {
    switch (m.getCommand()) {
      case WashingMessage.SPIN_OFF:
        speed = Speed.IDLE;
        io.setSpinMode(WashingIO.SPIN_IDLE);
        break;

      case WashingMessage.SPIN_SLOW:
        speed = Speed.SLOW;
        break;

      case WashingMessage.SPIN_FAST:
        speed = Speed.FAST;
        io.setSpinMode(WashingIO.SPIN_FAST);
        break;
    }
  }

  @Override
  protected void handleState() {
    if (speed == Speed.IDLE || speed == Speed.FAST) {
      return;
    } else if (dir == SpinDirection.LEFT) {
      dir = SpinDirection.RIGHT;
      io.setSpinMode(WashingIO.SPIN_RIGHT);
    } else {
      dir = SpinDirection.LEFT;
      io.setSpinMode(WashingIO.SPIN_LEFT);
    }
  }
}
