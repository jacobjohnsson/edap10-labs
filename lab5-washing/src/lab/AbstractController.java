package lab;

import wash.WashingIO;

public abstract class AbstractController extends MessagingThread<WashingMessage> {

  protected WashingIO io;
  protected int dt;

  public AbstractController(WashingIO io, int dt) {
    this.io = io;
    this.dt = dt;
  }

  @Override
  public void run() {
    try {
      while (true) {
        WashingMessage m = receiveWithTimeout(dt / Wash.SPEEDUP);

        if (m != null) {
          handleCommand(m);
        }

        handleState();

      }
    } catch(Exception e) {
      throw new Error(e);
    }
  }

  protected abstract void handleCommand(WashingMessage m);

  protected abstract void handleState();

}
