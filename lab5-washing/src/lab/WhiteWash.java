package lab;

import wash.WashingIO;

class WhiteWash extends AbstractWashingProgram {

  public WhiteWash(WashingIO io,
                   MessagingThread<WashingMessage> temp,
                   MessagingThread<WashingMessage> water,
                   MessagingThread<WashingMessage> spin) {
    super(io,temp, water, spin, "White wash");
  }

  @Override
  protected void program() throws InterruptedException {

    io.lock(true);

    water.send(new WashingMessage(this, WashingMessage.WATER_FILL, 10));
    WashingMessage ack = receive();

    spin.send(new WashingMessage(this, WashingMessage.SPIN_SLOW));
    temp.send(new WashingMessage(this, WashingMessage.TEMP_SET, 40));

    Thread.sleep(15 * 60000 / Wash.SPEEDUP);

    temp.send(new WashingMessage(this, WashingMessage.TEMP_SET, 60));

    Thread.sleep(30 * 60000 / Wash.SPEEDUP);

    temp.send(new WashingMessage(this, WashingMessage.TEMP_IDLE));
    water.send(new WashingMessage(this, WashingMessage.WATER_DRAIN));
    receive();

    for (int i = 0; i < 5; i++) {
      water.send(new WashingMessage(this, WashingMessage.WATER_FILL, 10));
      receive();
      Thread.sleep(2 * 60000 / Wash.SPEEDUP);

      water.send(new WashingMessage(this, WashingMessage.WATER_DRAIN));
      receive();
    }

    spin.send(new WashingMessage(this, WashingMessage.SPIN_FAST));
    Thread.sleep(5 * 60000 / Wash.SPEEDUP);

    spin.send(new WashingMessage(this, WashingMessage.SPIN_OFF));
    io.lock(false);
  }
}
