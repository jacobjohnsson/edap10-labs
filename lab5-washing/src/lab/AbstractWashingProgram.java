package lab;

import wash.WashingIO;

abstract class AbstractWashingProgram extends MessagingThread<WashingMessage> {
  protected WashingIO io;
  protected MessagingThread<WashingMessage> temp;
  protected MessagingThread<WashingMessage> water;
  protected MessagingThread<WashingMessage> spin;

  protected String name;

  public AbstractWashingProgram(WashingIO io,
                                MessagingThread<WashingMessage> temp,
                                MessagingThread<WashingMessage> water,
                                MessagingThread<WashingMessage> spin,
                                String name) {
    this.io = io;
    this.temp = temp;
    this.spin = spin;
    this.water = water;
    this.name = name;
  }

  @Override
  public void run() {
    try {

      System.out.println(name + " started");
      program();
      System.out.println(name + " finished" + "");

    } catch(InterruptedException e) {

      temp.send(new WashingMessage(this, WashingMessage.TEMP_IDLE));
      water.send(new WashingMessage(this, WashingMessage.WATER_IDLE));
      spin.send(new WashingMessage(this, WashingMessage.SPIN_OFF));
      System.out.println(name + " interrupted");

    } catch(Error e) {
      throw new Error(e);
    }
  }

  protected abstract void program() throws InterruptedException;
}
