package rsa;

import client.view.ProgressItem;

import javax.swing.*;

public class Tracker implements ProgressTracker {
  private int totalProgress = 0;
  private int prevPercent = -1;

  private ProgressItem progItem;
  private JProgressBar mainProgressBar;

  public Tracker(ProgressItem progItem, JProgressBar mainProgressBar) {
    this.progItem = progItem;
    this.mainProgressBar = mainProgressBar;
  }

  /**
   * Called by Factorizer to indicate progress. The total sum of
   * ppmDelta from all calls will add upp to 1000000 (one million).
   *
   * @param  ppmDelta   portion of work done since last call,
   *                    measured in ppm (parts per million)
   */
  @Override
  public void onProgress(int ppmDelta) {
    totalProgress += ppmDelta;
    int percent = totalProgress / 10000;

    // update mainProgressBar.
    SwingUtilities.invokeLater(() -> {
      int oldValue = mainProgressBar.getValue();
      mainProgressBar.setValue(oldValue + ppmDelta);
    });

    if (percent != prevPercent) {
      System.out.println(percent + "%");
      SwingUtilities.invokeLater(() -> {
        progItem.getProgressBar().setValue(totalProgress);
      });
      prevPercent = percent;
    }
  }
}
