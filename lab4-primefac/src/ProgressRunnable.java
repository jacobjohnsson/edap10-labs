import client.view.ProgressItem;
import rsa.Factorizer;
import rsa.ProgressTracker;
import rsa.Tracker;

import javax.swing.*;
import java.math.BigInteger;

public class ProgressRunnable implements Runnable {
  private ProgressItem progItem;
  private JProgressBar mainProgressBar;
  private JPanel progressList;
  private String message;
  private BigInteger n;

  public ProgressRunnable(ProgressItem progItem,
      JProgressBar mainProgressBar,
      JPanel progressList,
      String message,
      BigInteger n) {
    this.progItem = progItem;
    this.mainProgressBar = mainProgressBar;
    this.progressList = progressList;
    this.message = message;
    this.n = n;
  }

  @Override
  public void run() {
    ProgressTracker tracker = new Tracker(progItem, mainProgressBar);
    String plainText = Factorizer.crack(message, n, tracker);

    // When cracked update text
    SwingUtilities.invokeLater(() -> progItem.getTextArea().setText(plainText));
    System.out.println("ActionListener has decrypted. The message is \"" + plainText + "\"");

    // Add the remove button
    SwingUtilities.invokeLater(() -> {
      JButton removeButt = new JButton("Remove");
      progItem.add(removeButt);
      removeButt.addActionListener(e2 -> {
        SwingUtilities.invokeLater(() -> {
          System.out.println(Thread.currentThread().getName());

          progressList.remove(progItem);
          int oldValue = mainProgressBar.getValue();
          mainProgressBar.setValue(oldValue - 1000000);

          int oldMax2 = mainProgressBar.getMaximum();
          mainProgressBar.setMaximum(oldMax2 - 1000000);
        });
      });
    });
  }

}
