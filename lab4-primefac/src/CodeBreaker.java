import java.math.BigInteger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.*;

import client.view.ProgressItem;
import client.view.StatusWindow;
import client.view.WorklistItem;
import network.Sniffer;
import network.SnifferCallback;
import rsa.Factorizer;
import rsa.ProgressTracker;
import rsa.Tracker;

public class CodeBreaker implements SnifferCallback {

  private final JPanel workList;
  private final JPanel progressList;

  private final JProgressBar mainProgressBar;

  private final ExecutorService threadPool = Executors.newFixedThreadPool(4);

  // -----------------------------------------------------------------------

  private CodeBreaker() {

    StatusWindow w  = new StatusWindow();
    w.enableErrorChecks();

    workList        = w.getWorkList();
    progressList    = w.getProgressList();
    mainProgressBar = w.getProgressBar();

    new Sniffer(this).start();
  }

  // -----------------------------------------------------------------------

  public static void main(String[] args) throws Exception {

    /*
     * Most Swing operations (such as creating view elements) must be
     * performed in the Swing EDT (Event Dispatch Thread).
     *
     * That's what SwingUtilities.invokeLater is for.
     */

    SwingUtilities.invokeLater(() -> new CodeBreaker());
  }

  // -----------------------------------------------------------------------

  /** Called by a Sniffer thread when an encrypted message is obtained. */
  @Override
  public void onMessageIntercepted(String message, BigInteger n) {
    SwingUtilities.invokeLater(() -> {

      WorklistItem item = new WorklistItem(n, message);

      JButton button = new JButton("Break");
      item.add(button);
      workList.add(item);

      button.addActionListener(e1 -> {

        ProgressItem progItem = new ProgressItem(n, message);
        progressList.add(progItem);
        workList.remove(item);

        // update Main progress bar.
        int oldMax = mainProgressBar.getMaximum();
        mainProgressBar.setMaximum(oldMax + 1000000);

        threadPool.submit(new ProgressRunnable(progItem, mainProgressBar, progressList, message, n));
      });
    });
  }
}
