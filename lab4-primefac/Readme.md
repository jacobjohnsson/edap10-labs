# Lab 4

### I1:
  Progress is reported through callback method **onProgress(int ppmDelta)**.

  ppmDelta = parts per million.

### I2:
  CodeBreaker gets messages through the **onMessageIntercepted(String message, BigInteger n)** callback method from the sniffer Thread.
  These messages are currently outputted to the console.

### I5:
  The GUI freezez until the ActionEvent method is complete. The invokeLater call is put in queue and wont run until after the actionEvent is done, i.e crack before GUI udpate.

### I10:
  1. As many as there are threads in the pools (limited by cores in CPU).
  2. Yep! =D If not, it's because EDT does not have all resposibility for updating the GUI, i.e another thread has accessed the swing components.
  3.

# Reflektion
  * EDT
  * CodeBreaker (main)
  * Pool (2st)

  swing components är den delade datan som behöver uppdateras och presenteras.

  Swing blir upptagen med långa grejjer och kan därför inte hinna med andra anrop.

  Seperation mellan trådarna och arbetet trådarna skall utföra. Varje tråd kan således lösa oberoende uppgifter parallellt. Med begränsad storlek på poolen unviks onödiga kontextbyte.

  Eftersom swingtråden är den enda som pillar i swingkomponenterna blir de indirekt trådsäkra.
