# Lab6 notes

## Preperations

#### P2
>The C compiler checks for errors at compile time.  How are runtime errors (that is,incorrect pointers or invalid indices) handled in C?

Most of the time they're not. Such as index out of bounds which is not checked.

>What is a struct in C?

A primitive object with several primitive types in one structure. Similar to a Java class with nothing but public attributes.

> In a C file, some functions can only be called locally. In other words, you can be certain that they are never called from another C file. How can you spot such functions? (We assume functions pointers are not used.)

They are not declared in the header file and they're static.

#### P7
The unittests does *not* pass.

#### P8
>What is apthread_mutex_t? Can you think of anything similar in Java?

* A pthread_mutex_normal is essentially a new Lock().
* A pthread_mutex_recursive is a Semaphore(0).
* A pthread_mutex_

#### P9
Olåsta:
  * intset_create()
  * index()
  * find()
Låsta:
  * intset_add()
  * intset_contains()
  * intset_size()

#### P13
msg_store

## Reflection
The monitor (MessageStore) first iteration only had imported pthreads and did not actually use the library. We made the **mutual state** threadsafe by mutual exclusion with mutex lock. The problem was that we entered a deadlock, this because the busy-wait loop while holding the lock, see last method in MessageStore file. We solved this by adding an additional lock which can wait for a condition and release the lock while waiting. The methods changing the mutable state notifies this lock, and allows it to check its condition again.


Mutexes are similar to Semaphore(1). The condition and the mutex are separate and when you wish to notify, you provide the condition and the lock. Mutex + condition is almost synchronized in java.

The solution happened to be very similar in this case. It's somewhat more explicit in C.

At first the was a likely possibility for deadlock.

Because the busy-wait part still held on to the lock. Therefore no other thread could enter the monitor.

Because you want to know which threads should be notified.
