import lift.LiftView;

public class LiftThread extends Thread {
  private LiftView view;
  private LiftMonitor monitor;


  public LiftThread(LiftView view, LiftMonitor monitor) {
    this.view = view;
    this.monitor = monitor;
  }

  @Override
  public void run() {

    while (true) {
      Move move = monitor.nextMove();

      view.moveLift(move.here(), move.next());

      monitor.updateMove();
    }
    
  }

}
