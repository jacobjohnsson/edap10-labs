import lift.Passenger;

public class LiftMonitor {

  private int here, next, load;

  private int[] waitEntryUP;
  private int[] waitEntryDOWN;
  private int[] waitExit;

  private enum Direction {UP, DOWN}
  private Direction dir = Direction.UP;

  private int lockLift = 0;

  public LiftMonitor() {
    here = 1;
    next = 0;
    load = 0;
    waitEntryUP = new int[7];
    waitEntryDOWN = new int[7];
    waitExit = new int[7];
  }

  public synchronized void enterLift(int from, int dest) {
    Direction dirP = registerWaiting(from, dest);
    notifyAll();

    while (
      isLiftMoving() ||
      isWrongFloor(from) ||
      isFull() ||
      isWrongDirection(from, dest)
    ) {
      try {
        wait();
      } catch(Exception e) {
        throw new Error(e);
      }
    }

    passengerEnteringLift(dirP, dest);
    notifyAll();
  }

  public synchronized void exitLift(int dest) {

    while (
      isLiftMoving() ||
      isWrongFloor(dest)
    ) {
      try {
        wait();
      } catch(Exception e) {
        throw new Error(e);
      }
    }

    passengerExitingLift();
    notifyAll();
  }

  public synchronized void releaseLockLift() {
    lockLift--;
    notifyAll();
  }

  public synchronized void updateMove() {
    here = next;
    notifyAll();

    while (
      isEnteringLift() ||
      isExitingLift() ||
      noPassengersWaiting()
    ) {
      try {
        wait();
      } catch(Exception e) {
        throw new Error(e);
      }
    }

    liftAlgorithm();
  }

  public synchronized Move nextMove() {
    return new Move(here, next);
  }

  private Direction registerWaiting(int from, int dest) {
    Direction dirP = checkDir(from, dest);
    switch (dirP) {
      case UP:
        waitEntryUP[from]++;
        break;
      case DOWN:
        waitEntryDOWN[from]++;
        break;
    }
    return dirP;
  }

  private void passengerEnteringLift(Direction dirP, int dest) {
    lockLift++;
    load++;
    if (dirP == Direction.UP) {
      waitEntryUP[here]--;
    } else {
      waitEntryDOWN[here]--;
    }
    waitExit[dest]++;
  }

  private void passengerExitingLift() {
    lockLift++;
    load--;
    waitExit[here]--;
  }

  private boolean isLiftMoving() {
    return here != next;
  }

  private boolean isWrongFloor(int targetFloor) {
    return  here != targetFloor;
  }

  private boolean isFull() {
    return load > 3;
  }

  private boolean isWrongDirection(int from, int dest) {
    return checkDir(from, dest) != dir;
  }

  private Direction checkDir(int from, int dest) {
    Direction dirP;
    if (from > dest) {
      dirP = Direction.DOWN;
    } else {
      dirP = Direction.UP;
    }
    return dirP;
  }

  private void liftAlgorithm() {
    updateDirection();
    notifyAll();

    while (isEnteringLift()) {
      try {
        wait();
      } catch(Exception e) {
        throw new Error(e);
      }
    }

    updateNext();
  }

  private boolean isEnteringLift() {
    boolean result;
    if (dir == Direction.UP) {
      result = (waitEntryUP[here] > 0 && !isFull()) || lockLift > 0;
    } else {
      result = (waitEntryDOWN[here] > 0 && !isFull()) || lockLift > 0;
    }

    return result;
  }

  private boolean isExitingLift() {
    return (waitExit[here] > 0) || lockLift > 0;
  }

  private boolean noPassengersWaiting() {
    return isEmpty(waitEntryUP) &&
           isEmpty(waitEntryDOWN) &&
           isEmpty(waitExit);
  }

  private boolean isEmpty(int[] arr) {
    for (int passenger : arr) {
      if (passenger != 0) {
        return false;
      }
    }
    return true;
  }

  private void updateDirection() {
    switch (dir) {
      case UP:
        if (here == 6) {
          dir = Direction.DOWN;
        }
        break;
      case DOWN:
        if (here == 0) {
          dir = Direction.UP;
        }
        break;
    }
  }

  private void updateNext() {
    switch (dir) {
      case UP:
        next++;
        break;
      case DOWN:
        next--;
        break;
    }
  }
}
