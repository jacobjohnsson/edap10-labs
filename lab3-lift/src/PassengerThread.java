import lift.Passenger;

public class PassengerThread extends Thread {
  private Passenger view;
  private LiftMonitor monitor;
  private boolean isInLift;

  public PassengerThread(Passenger view, LiftMonitor monitor) {
    this.view = view;
    this.monitor = monitor;
  }

  @Override
  public void run() {
    view.begin();

    monitor.enterLift(
      view.getStartFloor(),
      view.getDestinationFloor());
    view.enterLift();
    monitor.releaseLockLift();

    monitor.exitLift(view.getDestinationFloor());
    view.exitLift();
    monitor.releaseLockLift();

    view.end();
  }
}
