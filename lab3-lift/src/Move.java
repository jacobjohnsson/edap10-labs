public class Move {
  private int here;
  private int next;

  public Move(int here, int next) {
    this.here = here;
    this.next = next;
  }

  public int here() {
    return here;
  }

  public int next() {
    return next;
  }
}
