
import lift.LiftView;
import lift.Passenger;

import java.util.Random;

public class Main {

  public static void main(String[] args) {
    int nbrPassengers = 30;

    LiftView view = new LiftView();
    LiftMonitor monitor = new LiftMonitor();

    LiftThread lift = new LiftThread(view, monitor);
    lift.setName("Lift");
    lift.start();

    while (true) {

      for (int i = 0; i < nbrPassengers; i++) {
        try {
          Thread.sleep(new Random().nextInt(1000) + 500);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        Passenger passangerView = view.createPassenger();
        PassengerThread p =
          new PassengerThread(passangerView, monitor);
        p.setName("Passenger: " + i);
        p.start();
      }

      try {
        Thread.sleep(1000 * 20);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    
  }
}
