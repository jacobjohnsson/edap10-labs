## Design.
#### Shared data
 1. What shared data will you need in your monitor?
    * Current position.
    * Next position.
    * List of passengers waiting at the different floors.
    * List of passengers inside the lift and their destination floor.
    * number of passengers inside lift.


|  Klasser | Kommentar  |
|---|---|
| Main | Du'h |
| Monitor | ska innehålla ovanstående attribut. |
| Passenger | Thread för passagerare. |
| Lift | Hissens tråd. |
