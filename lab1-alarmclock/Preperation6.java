class Preperation6 {
  public static void main(String[] args) {
    try {

      final long t0 = System.currentTimeMillis();
      long t1;
      long totalTime;
      long diffTime;
      long expectedTime = 0;

      while (true) {
        t1 = System.currentTimeMillis();
        totalTime = t1 - t0;
        diffTime = expectedTime * 1000 - totalTime;
        expectedTime++;
        System.out.println(totalTime);
        Thread.sleep(1000 + diffTime);
      }
    } catch(Exception e) {
      throw new Error(e);
    }

  }
}
