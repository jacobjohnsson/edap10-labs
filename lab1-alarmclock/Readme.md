### 1.1.4 Design tasks

  1. En tråd som uppdaterar klockan varje sekund. Denna kan också vara ansvarig för att kolla om alarmet borde ringa.

  2. CurrentTime och alarmTime. Dessa ligger i SharedData

  > @ThreadSafe  
  > public class AlarmTime {  
  >   @GuardedBy("this") private int alarmTime;  
  >  
  >   public synchronized void set(int time) {  
  >     this.alarmTime = time;  
  >   }
  >   
  >   public synchronized int get() {
  >     return alarmTime;
  >   }  
  > }  

  (men skulle det inte funka att sätta alarmTime som public volatile och skita i set & get? kass design?)

  3.  
    * main: setCurrentTime(int time), setAlarmTime(int time), activate/deactivate alarm
    * timer: get(), timeEquals(int time)
<<<<<<< HEAD

  4.

  5. I getUserInput för att undvika race mellan input.

  6. a)
  1 - Det är inte säkert att tråden får CPU tid direkt efter Thread.sleep. Den blir bara Ready, inte Running.
  2 - ?

### Frågor

1. I punkt 6 har vi tänkt att thread.sleep inte garanterar att tråden startas igen efter sleep. Den andra är vi dock osäker på, schemaläggning?


### Bash stuff
javac Preperation6.java && java Preperation6
javac FormatTest.java && java FormatTest
=======

  4. I vår SharedData klass.

  5. I getUserInput för att undvika race mellan input.

  6. a)
  1 - Det är inte säkert att tråden får CPU tid direkt efter Thread.sleep. Den blir bara Ready, inte Running.
  2 - ?

### Frågor

1. I punkt 6 har vi tänkt att thread.sleep inte garanterar att tråden startas igen efter sleep. Den andra är vi dock osäker på, schemaläggning?


### Bash stuff
javac Preperation6.java && java Preperation6
>>>>>>> 069f38d007f81894df0ee1b24e980caae1095210
