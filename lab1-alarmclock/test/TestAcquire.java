import java.util.Scanner;
import java.util.concurrent.Semaphore;

public class TestAcquire {

  public static void main(String[] args) {
    Semaphore sharedSem = new Semaphore(0);

    T1 thread1 = new T1(sharedSem);
    new Thread(thread1).start();

    Scanner scan = new Scanner(System.in);

    while(true) {
      int input = scan.nextInt();
      if(input == 1) {
        sharedSem.release();
      } else {
        try {
          sharedSem.acquire();
        } catch (InterruptedException e) {
          throw new Error(e);
        }
      }
    }
  }

  public static class T1 implements Runnable {
    private Semaphore sharedSem;
    private int count = 0;

    public T1(Semaphore sharedSem) {
      this.sharedSem = sharedSem;
    }

    @Override
    public void run() {
      while(true) {
        try {
          sharedSem.acquire();
        } catch(Exception e) {
          throw new Error(e);
        }

        count++;
        System.out.println(count);
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          throw new Error(e);
        }
      }
    }
  }
}
