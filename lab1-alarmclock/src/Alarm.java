import java.util.concurrent.Semaphore;

public class Alarm implements Runnable {
  private int nbrPings = 20;
  private int tmp = 0;
  private Semaphore tickSignaler;
  private ClockData data;


  public Alarm(Semaphore tickerSignaler, ClockData data) {
    this.tickSignaler = tickerSignaler;
    this.data = data;
  }


  @Override
  public void run() {
    boolean isAlive = true;

    while(isAlive) {

      try {
        tickSignaler.acquire();
        // System.out.println(tmp);
        // tmp++;
        // The Alarm is syncronised with Ticker.
      } catch (InterruptedException e) {
        throw new Error(e);
      }

      if (nbrPings > 0) {
        data.ping();
        nbrPings--;
      } else {
        data.snooze();
        isAlive = false;
      }
    }
  }

}
