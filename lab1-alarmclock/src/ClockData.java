/*  All tid som sparas i denna klassen är i sekunder.
 *  Det är först på vägen ut/in som det formatteras.
 * currentTime -> timeForDisplay
 * 86390 -> 235950
 * 7190 -> 15950
 */

import clock.ClockOutput;
import java.util.concurrent.Semaphore;

public class ClockData {
  private int currentTime;
  private int alarmTime = 0;

  private boolean isAlarmToggled = false;
  private boolean isRinging = false;
  private Semaphore tickSignaler = new Semaphore(0);
  private Thread alarmThread = new Thread(new Alarm(tickSignaler, this));

  private ClockOutput output;
  private Semaphore mutex = new Semaphore(1);

  // ------------- Constructors ------------------

  public ClockData(int initTime, ClockOutput output) {
    this.currentTime = Formatter.toSeconds(initTime);
    this.output = output;
    initClock();
  }

  public ClockData(ClockOutput output) {
    this.currentTime = 0;
    this.output = output;
  }

  // ----------------------------------------------

  public void incrementTime() {
    exclude();
    currentTime++;
    checkTimeOverflow();

    if (isAlarmToggled || isRinging) {
      handleAlarm();
    }

    int cTime = currentTime;
    mutex.release();

    output.displayTime(Formatter.toDisplay(cTime));
  }

  public void toggle() {
    exclude();
    isAlarmToggled = !isAlarmToggled;
    output.setAlarmIndicator(isAlarmToggled);
    mutex.release();
  }

  public void snooze() {
    exclude();
    isRinging = false;
    alarmThread = new Thread(new Alarm(tickSignaler, this));
    mutex.release();
  }

  public void ping(){
    exclude();
    if (isRinging) {
      output.alarm();
    }
    mutex.release();
  }
  
  private void handleAlarm() {
    if (currentTime == alarmTime) {
      isRinging = true;
      alarmThread.start();
      tickSignaler.release();
    } else if(isRinging) {
      tickSignaler.release();
    }
  }


  public void setAlarmTime(int newTime) {
    int formattedInput = Formatter.toSeconds(newTime);
    exclude();
    alarmTime = formattedInput;
    mutex.release();
  }

  public void setCurrentTime(int newTime) {
    int formattedInput = Formatter.toSeconds(newTime);
    exclude();
    currentTime = formattedInput;
    mutex.release();
  }

  private void exclude() {
    try {
      mutex.acquire();
    } catch(Exception e) {
      throw new Error(e);
    }
  }

  private void checkTimeOverflow() {
    if (currentTime >= 86400) {
      currentTime = 0;
    }
  }

  private void initClock() {
    exclude();
    output.displayTime(currentTime);
    output.setAlarmIndicator(isAlarmToggled);
    mutex.release();
  }
}
