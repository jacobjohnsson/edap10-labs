import java.util.concurrent.Semaphore;

public class Ticker implements Runnable {
  private ClockData clockData;

  public Ticker(ClockData clockData) {
    this.clockData = clockData;
  }

  @Override
  public void run() {
    final long t0 = System.currentTimeMillis();
    long t1;
    long totalTime;
    long diffTime;
    long expectedTime = 0;

    while (true) {
      t1 = System.currentTimeMillis();
      totalTime = t1 - t0;
      diffTime = expectedTime * 1000 - totalTime;
      expectedTime++;

      clockData.incrementTime();

      try {
        Thread.sleep(1000 + diffTime);
      } catch(Exception e) {
        throw new Error(e);
      }
    }
  }
}
