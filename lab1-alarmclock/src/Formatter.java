public class Formatter {

  public static int toDisplay(int time) {
    int hours = time / 3600;
    int minutes = (time % 3600) / 60;
    int seconds = (time % 3600) % 60;

    return hours * 10000 + minutes * 100 + seconds;
  }

  public static int toSeconds(int time) {
    int hours = time / 10000;
    int minutes = (time % 10000) / 100;
    int seconds = time % 100;
    
    return (hours * 3600 + minutes * 60 + seconds);
  }
}
