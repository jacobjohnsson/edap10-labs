import clock.ClockOutput;

public class RingingBehavior {
  protected boolean isActive = false;
  protected boolean isRinging = false;
  protected int ringCount = 0;
  ClockOutput output;

  public RingingBehavior(ClockOutput output) {
    this.output = output;
  }

  // race vid output?
  public void toggle() {
    isActive = !isActive;
    output.setAlarmIndicator(isActive);
  }

  public void snooze() {
    isRinging = false;
  }

  public void tick(int currentTime, int alarmTime) {
    if (currentTime == alarmTime && isActive) {
      isRinging = true;
    }

    if (isRinging) {
      Thread alarmThread = new Thread(() -> output.alarm());
      alarmThread.start();
      ringCount++;
      if (ringCount > 19) {
        ringCount = 0;
        isRinging = false;
      }
    }
  }
}
