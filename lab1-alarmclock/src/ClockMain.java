import java.util.concurrent.Semaphore;

import clock.ClockInput;
import clock.ClockInput.UserInput;
import clock.ClockOutput;
import emulator.AlarmClockEmulator;

// ------ Fusk ------
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
// ------ Slut på fusk ------

public class ClockMain {

    public static void main(String[] args) throws InterruptedException {
        AlarmClockEmulator emulator = new AlarmClockEmulator();

        ClockInput input  = emulator.getInput();
        ClockOutput out = emulator.getOutput();

        Semaphore inputSignaler = input.getSemaphore();

        ClockData data = new ClockData(235957, out);
        Ticker ticker = new Ticker(data);
        Thread timerThread = new Thread(ticker);
        timerThread.start();

        // ------ Fusk ------
        // ScheduledExecutorService executerService = Executors.newSingleThreadScheduledExecutor();
        // executerService.scheduleAtFixedRate(() -> data.incrementTime(), 0, 1, TimeUnit.SECONDS );
        // ------ Slut på fusk ------


        while (true) {
            inputSignaler.acquire();             // wait for user input
            data.snooze();

            UserInput userInput = input.getUserInput();
            int choice = userInput.getChoice();
            int value = userInput.getValue();

            switch (choice) {
              // Change current time.
              case 1:
                data.setCurrentTime(value);
                break;
              // Change current Alarm
              case 2:
                data.setAlarmTime(value);
                break;
              // Toggle alarm.
              case 3:
                data.toggle();

                break;
              default:
                break;
            }

            System.out.println("choice = " + choice + "  value=" + value);
        }
    }
}
